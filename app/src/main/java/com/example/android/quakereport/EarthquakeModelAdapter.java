package com.example.android.quakereport;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by mustaphar on 3/14/17.
 */

public class EarthquakeModelAdapter extends ArrayAdapter<EarthquakeModel> {

    public EarthquakeModelAdapter(Activity activity, ArrayList<EarthquakeModel> data) {
        super(activity, 0, data);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View listItemView = convertView;
        if (listItemView == null) {
            listItemView = LayoutInflater.from(getContext()).inflate(R.layout.list_item, parent, false);
        }

        EarthquakeModel model = getItem(position);

        TextView tvStrength = (TextView) listItemView.findViewById(R.id.strength);
        tvStrength.setText(Double.toString(model.getStrength()));

        TextView tvPlace = (TextView) listItemView.findViewById(R.id.place);
        tvPlace.setText(model.getPlace());

        TextView tvDate = (TextView) listItemView.findViewById(R.id.date);
        tvDate.setText(model.getDate());

        return listItemView;
    }
}
