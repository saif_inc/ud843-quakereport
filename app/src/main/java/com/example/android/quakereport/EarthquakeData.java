package com.example.android.quakereport;

import java.util.ArrayList;

/**
 * Created by mustaphar on 3/14/17.
 */

public class EarthquakeData {
    ArrayList<EarthquakeModel> earthquakes = new ArrayList<>();

    ArrayList<EarthquakeModel> getEarthquakeData() {
        return earthquakes;
    }

    EarthquakeData() {
        earthquakes.add(new EarthquakeModel(7.2, "San Francisco", "Feb 2, 2016"));
        earthquakes.add(new EarthquakeModel(6.1, "London", "July 20, 2015"));
        earthquakes.add(new EarthquakeModel(3.9, "Tokyo", "Nov 10, 2014"));
        earthquakes.add(new EarthquakeModel(5.4, "Mexico City", "May 3, 2014"));
        earthquakes.add(new EarthquakeModel(2.8, "Moscow", "Jul 31, 2013"));
        earthquakes.add(new EarthquakeModel(4.9, "Rio de Janeiro", "Aug 19, 2012"));
        earthquakes.add(new EarthquakeModel(1.6, "Paris", "Oct 30, 2011"));
    }
}