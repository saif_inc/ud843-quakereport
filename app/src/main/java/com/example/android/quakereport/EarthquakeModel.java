package com.example.android.quakereport;

import java.util.Date;

/**
 * Created by mustaphar on 3/14/17.
 */

public class EarthquakeModel {
    double strength;
    String place;
    String date;

    EarthquakeModel(double strength, String place, String date) {
        this.strength = strength;
        this.place = place;
        this.date = date;
    }

    public double getStrength() {
        return strength;
    }

    public String getPlace() {
        return place;
    }

    public String getDate() {
        return date;
    }

}